<?php
/**
 * Plugin Name:       Wisdm Datatable
 * Description:       A plugin to add data into table
 * Requires at least: 5.7
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            Ram
 * Text Domain:       Datatable
 *
 */

 if(!defined('ABSPATH')){
    header("location: /blog");
    die();
 }

 
  //hook activation
function datatable_wisdmlab_activation(){
    global $wpdb, $table_prefix;
    $wp_emp = $table_prefix. 'wisdm';

    $q = "CREATE TABLE IF NOT EXISTS `$wp_emp` (`ID` INT NOT NULL AUTO_INCREMENT,`name` VARCHAR(50)
    NOT NULL, `email` VARCHAR(100) NOT NULL, `status` VARCHAR(100) NOT NULL ,
    PRIMARY KEY (`ID`))";
    $wpdb->print_error();
    $wpdb->query($q);

    // $q = $wpdb->prepare("INSERT INTO `$wp_emp` (`name`, `email`, `status`) VALUES (%s, %s, %s)", 'Rameshwar', 'mangnale@gmail.com', 'yes');
    // $wpdb->query($q);

    $data = array(
        'name' => 'Rameshwar',
        'email' => 'mangnale1@gmail.com',
        'status' => 'yes'
    );

    $wpdb->insert($wp_emp,$data);

}
register_activation_hook(__FILE__, 'datatable_wisdmlab_activation');

//deactivation 
function datatable_wisdmlab_deactivation(){
    global $wpdb, $table_prefix;
    $wp_emp = $table_prefix. 'wisdm';

    $q = "TRUNCATE `$wp_emp`";
    $wpdb->query($q);

}
register_deactivation_hook(__FILE__, 'datatable_wisdmlab_deactivation');

//if function exit , secure from naming conflix
if(!function_exists('wpwisdm_my_plugin_function')){
    function wpwisdm_my_plugin_function(){
//
    }
}
